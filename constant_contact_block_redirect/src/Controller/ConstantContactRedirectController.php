<?php

namespace Drupal\constant_contact_block_redirect\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * The redirect controller.
 *
 * @package Drupal\constant_contact_block_redirect\Controller
 */
class ConstantContactRedirectController extends ControllerBase {

  /**
   * The default redirect route.
   *
   * @return array
   *   The render array.
   */
  public function index(){

    return array(
      '#theme' => 'constant_contact_block_redirect',
    );
  }
}
