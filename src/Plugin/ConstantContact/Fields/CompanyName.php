<?php

namespace Drupal\constant_contact_block\Plugin\ConstantContact\Fields;

use Drupal\constant_contact_block\Plugin\ConstantContactFieldInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class for creating Company Name field.
 *
 * @ConstantContactField(
 *   id = "company_name",
 *   label = @Translation("Company Name"),
 *   description = @Translation("Subscriber Company Name"),
 * )
 */
class CompanyName implements ConstantContactFieldInterface {

  use StringTranslationTrait;
  /**
   * {@inheritdoc}
   */
  public function field() {
    return array(
      '#title' => $this->t('Company Name'),
      '#type' => 'textfield',
      '#maxlength' => 60,
      '#required' => TRUE,
    );
  }
}
