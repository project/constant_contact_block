<?php

namespace Drupal\constant_contact_block\Plugin\ConstantContact\Fields;

use Drupal\constant_contact_block\Plugin\ConstantContactFieldInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class for creating First Name field.
 *
 * @ConstantContactField(
 *   id = "first_name",
 *   label = @Translation("First Name"),
 *   description = @Translation("Subscriber First Name"),
 * )
 */
class FirstName implements ConstantContactFieldInterface {

  use StringTranslationTrait;
  /**
   * {@inheritdoc}
   */
  public function field() {
    return array(
      '#title' => $this->t('First Name'),
      '#type' => 'textfield',
      '#maxlength' => 60,
      '#required' => TRUE,
    );
  }
}
