<?php

namespace Drupal\constant_contact_block\Plugin\ConstantContact\Fields;

use Drupal\constant_contact_block\Plugin\ConstantContactFieldInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class for creating Last Name field.
 *
 * @ConstantContactField(
 *   id = "last_name",
 *   label = @Translation("Last Name"),
 *   description = @Translation("Subscriber Last Name"),
 * )
 */
class LastName implements ConstantContactFieldInterface {

  use StringTranslationTrait;
  /**
   * {@inheritdoc}
   */
  public function field() {
    return array(
      '#title' => $this->t('Last Name'),
      '#type' => 'textfield',
      '#maxlength' => 60,
      '#required' => TRUE,
    );
  }
}
