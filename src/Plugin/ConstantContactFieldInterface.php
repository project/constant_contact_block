<?php

namespace Drupal\constant_contact_block\Plugin;

/**
 * Defines the required interface for all Constant Contact Subscriber Field plugins.
 *
 * @package Drupal\constant_contact_block
 */
interface ConstantContactFieldInterface {
  /**
   * Returns the form element.
   *
   * @return array
   *   Array defining the form element.
   */
  public function field();
}
