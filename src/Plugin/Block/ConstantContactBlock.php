<?php

namespace Drupal\constant_contact_block\Plugin\Block;

use Drupal\constant_contact_block\ConstantContactFieldPluginManager;
use Drupal\constant_contact_block\Form\ConstantContactForm;
use Drupal\constant_contact_block\services\ConstantContactDataInterface;
use Drupal\constant_contact_block\services\ConstantContactFieldsInterface;
use Drupal\constant_contact_block\services\ConstantContactInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilder;

/**
 * Provides a 'constant contact' block.
 *
 * @Block(
 *   id = "constant_contact",
 *   admin_label = @Translation("Constant Contact"),
 *   category = @Translation("Custom constant contact block")
 * )
 */
class ConstantContactBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The constantContactManager service.
   *
   * @var ConstantContactInterface
   */
  protected $constantContactManager;
  /**
   * The constantContactDataService.
   *
   * @var ConstantContactDataInterface
   */
  protected $constantContactDataService;
  /**
   * The constantContactFieldService.
   *
   * @var ConstantContactFieldsInterface
   */
  protected $constantContactFieldService;
  /**
   * The form builder service.
   *
   * @var FormBuilder
   */
  protected $formBuilderService;

  /**
   * The constant contact field plugin manager.
   *
   * @var ConstantContactFieldPluginManager
   */
  protected $fieldPluginManager;
  /**
   * The constant_contact_block config object.
   *
   * @var ImmutableConfig
   */
  protected $constantContactConfig;

  protected $constantContactBlockConfig;

  protected $contactLists;
  /**
   * The messenger service.
   *
   * @var Messenger
   */
  protected $messenger;
  private $lists = [];
  /**
   * The field machine name.
   *
   * @var string
   */
  private $machineName;

  /**
   * ConstantContactBlock constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   Plugin definitions.
   * @param ConstantContactInterface $constantContactManager
   *   The constant contant manager service.
   * @param ConstantContactDataInterface $constantContactDataService
   *   The constant contanct data service.
   * @param ConfigFactory $configFactory
   *   The configuration object.
   * @param Messenger $messenger
   *   The messenger service.
   * @param ConstantContactFieldsInterface $constantContactFieldService
   *   The constantContactFieldService.
   * @param FormBuilder $formBuilderService
   *   The form builder service.
   */
  public function __construct(array $configuration,
  $plugin_id, $plugin_definition, ConstantContactInterface $constantContactManager,
                                ConstantContactDataInterface $constantContactDataService, ConfigFactory $configFactory,
                              Messenger $messenger, ConstantContactFieldsInterface $constantContactFieldService,
  FormBuilder $formBuilderService, ConstantContactFieldPluginManager $fieldPluginManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fieldPluginManager = $fieldPluginManager;
    $this->constantContactManager = $constantContactManager;
    $this->constantContactDataService = $constantContactDataService;
    $this->messenger = $messenger;
    $this->machineName = $this->getMachineNameSuggestion();
    $this->formBuilderService = $formBuilderService;
    $this->constantContactFieldService = $constantContactFieldService;
    $this->constantContactConfig = $configFactory->get('constant_contact_block.constantcontantconfig');
    //@todo to be removed with deprecated code.
    $this->constantContactBlockConfig = $configFactory->getEditable('constant_contact_block.constantcontantconfig');

    if ($this->constantContactConfig->get('data_src')) {
      $this->contactLists = $constantContactManager->getContactLists();
      $this->contactLists = json_decode($this->contactLists);
    }
    else {
      $this->contactLists = $constantContactDataService->getContactLists();
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
        $container->get('constant_contact_block.manager_service'),
        $container->get('constant_contact_block.data_manager'),
        $container->get('config.factory'),
        $container->get('messenger'),
        $container->get('constant_contact_block.fields_manager'),
        $container->get('form_builder'),
        $container->get('plugin.manager.constant_contact_block_fields')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $fields = $this->constantContactFieldService->loadFields();
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    if (isset($config['machine_name'])) {
      $this->machineName = $config['machine_name'];
    }

    $listOptions = [];
    foreach ($this->contactLists as $contactList) {
      $listOptions[$contactList->id] = $contactList->name;
    }
    $this->contactLists = $listOptions;

    $emailLists = [];

    if (isset($config['cc_email_' . $this->machineName])) {
      $emailLists = $config['cc_email_' . $this->machineName];
    }
    //@todo to be removed with deprecated code.
    $selectedFields = [];
    if (isset($config['cc_fields_' . $this->machineName])) {
      $selectedFields = $config['cc_fields_' . $this->machineName];
      $form['fields_warning'] = array(
        '#type' => 'markup',
        '#markup' => "<div>Field options in Block configuration to be deprecated 8.x-1.0-beta6 and is removed from 8.x-2.0. 
Use <a href=/admin/config/constant_contact_block/constantcontantconfig>configure fields</a></div>",
      );

    }
    //@todo to be removed with deprecated code.
    foreach ($fields as $field => $value) {
      $form[$field] = [
        '#title' => $value['title'],
        '#type' => 'checkbox',
        '#default_value' => in_array($field, $selectedFields) ? 1 : 0,
      ];
    }

    $form['cc_email_' . $this->machineName] = [
      '#title' => $this->t('Email lists'),
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#description' => $this->t('Constant contact email lists available.'),
      '#options' => $listOptions,
      '#default_value' => $emailLists,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $machineName = $config['machineName'];
    $constantContactForm = new ConstantContactForm($machineName, $config['constant_contact_block_form_' . $machineName],
      $this->messenger, $this->constantContactFieldService, $this->constantContactManager, $this->fieldPluginManager, $this->constantContactConfig);
    $form = $form = $this->formBuilderService->getForm($constantContactForm);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $fields = $this->constantContactFieldService->loadFields();
    $configFields = array();
    $selectedFields = [];
    foreach ($fields as $field => $value) {
      $selectedField = $form_state->getValue($field);
      $configFields[$field] = $selectedField;
      if ($selectedField == 1) {
        $configFields[$field] = $field;
        array_push($selectedFields, $field);
      }
    }
    //@todo to be removed with deprecated code.
    $this->constantContactBlockConfig->set('fields', $configFields)->save();

    $this->setConfigurationValue('machineName', $this->machineName);
    $selectedLists = $form_state->getValue('cc_email_' . $this->machineName);

    foreach ($selectedLists as $selectedList => $value) {
      if ($value != 0) {
        $this->lists[$value] = $this->contactLists[$value];
      }
    }
    $this->setConfigurationValue('cc_fields_' . $this->machineName, $selectedFields);
    $this->setConfigurationValue('cc_email_' . $this->machineName, $selectedLists);
    $this->setConfigurationValue('constant_contact_block_form_' . $this->machineName, ['lists' => $this->lists, 'fields' => $selectedFields]);
  }

}
