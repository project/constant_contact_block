<?php

namespace Drupal\constant_contact_block\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\constant_contact_block\ConstantContactFieldPluginManager;
use Drupal\constant_contact_block\items\Contact;
use Drupal\constant_contact_block\items\EmailAddress;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\constant_contact_block\services\ConstantContactFieldsInterface;
use Drupal\constant_contact_block\services\ConstantContactInterface;


/**
 * Creates a form to create constant contact list on submission.
 */
class ConstantContactForm extends FormBase {

  /**
   * The messenger service.
   *
   * @var Messenger
   */
  protected $messenger;
  /**
   * The constant contact field service.
   *
   * @var ConstantContactFieldsInterface
   */
  protected $constantContactFieldService;
  /**
   * The constant contact manager service.
   *
   * @var ConstantContactInterface
   */
  protected $constantContactManager;

  /**
   * The constant contact field plugin manager.
   *
   * @var ConstantContactFieldPluginManager
   */
  protected $fieldPluginManager;
  protected $subscriberFields;

  private $fields = [];
  /**
   * The form id.
   *
   * @var string
   */
  private $formId;

  /**
   * Constructs a new ConstantContactForm.
   *
   * @param string $formId
   *   The form id.
   * @param array $fields
   *   The fields to add to the form.
   * @param Messenger $messenger
   *   The messenger service.
   * @param ConstantContactFieldsInterface $constantContactFieldService
   *   The constantContactFieldService.
   * @param ConstantContactInterface $constantContactManager
   *   The constantContactManager service.
   * @param ConstantContactFieldPluginManager $fieldPluginManager
   * @param ImmutableConfig $constantContactConfig
   */
  public function __construct($formId, array $fields, Messenger $messenger, ConstantContactFieldsInterface $constantContactFieldService,
                              ConstantContactInterface $constantContactManager, ConstantContactFieldPluginManager $fieldPluginManager, ImmutableConfig $constantContactConfig) {
    $this->formId = $formId;
    $this->fields = $fields;
    $this->messenger = $messenger;
    $this->constantContactFieldService = $constantContactFieldService;
    $this->constantContactManager = $constantContactManager;
    $this->fieldPluginManager = $fieldPluginManager;
    $this->subscriberFields = $constantContactConfig->get('fields');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'constant_contact_block_form_' . $this->formId;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $parameter = NULL) {

    foreach ($this->subscriberFields as $subscriberField){
      if ($subscriberField){
        try{
          $instance = $this->fieldPluginManager->createInstance($subscriberField);
          $form[$subscriberField] = $instance->field();
        }
        catch (PluginException $e) {
          //@todo replace by using DI.
          \Drupal::logger('constant_contact_block')->error($e->getMessage());
        }
      }
    }
    //@todo to be removed with deprecated code.
    $fields = $this->constantContactFieldService->loadFields();
    $selectedFields = $this->fields['fields'];

    foreach ($selectedFields as $selectedField) {
      $form[$selectedField] = [
        '#type' => $fields[$selectedField]['type'],
        '#title' => $fields[$selectedField]['title'],
        '#required' => $fields[$selectedField]['required'],
      ];
    }



    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email:'),
      '#required' => TRUE,
    ];

    $form['email_lists'] = [
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#title' => t('Lists:'),
      '#options' => $this->fields['lists'],
      '#required' => TRUE,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Register'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('email');
    $selectedLists = $form_state->getValue('email_lists');

    $lists = [];
    foreach ($selectedLists as $selectedList => $value) {
      if ($value != 0) {
        $listObj = new \stdClass();
        $listObj->id = (string) $selectedList;
        array_push($lists, $listObj);
      }
    }
    $submittedFields = array();
    //@todo to be removed with deprecated code.
    $fields = $this->fields['fields'];
    foreach ($fields as $field){
      $submittedFields[$field] = $form_state->getValue($field);
    }
    //@todo to be added to replace above deprecated code.
    /*foreach ($this->subscriberFields as $field) {
      if($field){
        $submittedFields[$field] = $form_state->getValue($field);
      }
    }*/

    $contact = $this->createConstantContact($submittedFields, $fields, [new EmailAddress($email)], $lists);


    /*$contact = new Contact($submittedFields['first_name'], $submittedFields['last_name'], $submittedFields['company_name'],
        'ACTIVE', [new EmailAddress($email)], $lists);*/

    $checkContact = $this->constantContactManager->checkContactExistsByEmail($email);
    $message = NULL;

    if (is_null($checkContact)){
        $this->messenger->addMessage('Error adding or updating email lists');
    }
    else{
        if (empty($checkContact)) {
            //$message = $this->constantContactManager->addContact($contact);
            $message = $this->constantContactManager->addConstantContact($contact);
        }
        else {
            $message = $this->constantContactManager->updateContant($checkContact, $lists);
        }
        if (is_null($message)){
            $this->messenger->addMessage('Error adding you to email lists');
        }
        else{
            if (!empty($message)) {
                $this->messenger->addMessage('You have been added to the email lists');
            }
        }
    }
  }

  /**
   * Creates a constant contact object.
   *
   * @param array $param
   *   The selected fields.
   *
   * @param array $fields
   *   The fields.
   *
   * @param $email_addresses
   *   The email address to add to a list.
   *
   * @param $lists
   *   The email lists
   *
   * @param string $status
   *   The status of the registered user.
   *
   * @return \stdClass
   *   The constant contact object.
   */
  private function createConstantContact(array $param, array $fields, array  $email_addresses, $lists, $status = 'ACTIVE'){
    $constantContact = new \stdClass();
    $constantContact->email_addresses = $email_addresses;
    $constantContact->status = $status;
    $constantContact->lists = $lists;

    foreach ($fields as $field){
      if (array_key_exists($field, $param)){
        $constantContact->{$field } = $param[$field];
      }
    }

    return $constantContact;
  }
}
