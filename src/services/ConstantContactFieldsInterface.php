<?php

namespace Drupal\constant_contact_block\services;

/**
 * Provides an interface defining the constant contact form fields.
 *
 * @deprecated in 8.x-1.0-beta6 and to be removed in 8.x-2.0.
 */
interface ConstantContactFieldsInterface {

  /**
   * Loads constant contact fields from a yaml file.
   *
   * @return array
   *   Returns an array of constant contact fields
   */
  public function loadFields();

}
