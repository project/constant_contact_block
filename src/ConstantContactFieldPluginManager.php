<?php

namespace Drupal\constant_contact_block;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The subscriber constant contact field plugin manager.
 */
class ConstantContactFieldPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, $plugin_definition_annotation_name = 'Drupal\Component\Annotation\Plugin') {
    parent::__construct('Plugin/ConstantContact/Fields', $namespaces, $module_handler, 'Drupal\constant_contact_block\Plugin\ConstantContactFieldInterface', 'Drupal\constant_contact_block\Annotation\ConstantContactField');
    $this->alterInfo('constant_contact_block_field_info');
    $this->setCacheBackend($cache_backend, 'constant_contact_block_field');
  }
}
