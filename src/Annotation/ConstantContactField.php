<?php
namespace Drupal\constant_contact_block\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Constant Contact Subscriber Field annotation object.
 *
 * @Annotation
 */
class ConstantContactField extends Plugin{
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the Constant Contact Subscriber Field.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * The description shown to users.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;
}
